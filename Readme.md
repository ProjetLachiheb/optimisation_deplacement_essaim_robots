# Evaluation de systèmes de sélection et de systèmes de mutation dans le but d’optimiser le déplacement d’un groupe de robots.


## Renseignements

Encadrant:

* Nicolas Bredeche

Etudiantes :

* Lachiheb 	Sarah 
* Leroy 		Cassandre

Sorbonne Université (UPMC) 2018

## Préambule

Dans le cadre du projet semestrielle du master 1 Androïde , nous avons pour but d’optimiser les paramètres d’un algorithme génétique dans l’objectif de l’appliquer à des robots réels. La première partie de notre projet est exécutée sur un simulateur nommé « pySpiteWorld » tandis que la deuxième est en conditions réelles.

Correspondant à notre première partie, le logiciel « pySpiteWorld » est un simulateur permettant d’observer des robots, dans un espace modélisé avec des obstacles pour tester leurs capacités à éviter des murs sur leurs trajectoires. Ce logiciel, dans le cadre de notre projet, va nous permettre d’évaluer des opérateurs de sélection et de mutation, pour en extraire les meilleurs composantes qui correspondraient aux mieux à l’évaluation et à l’adaptation des robots Thymio en conditions réelles. Et en deuxième partie, nous utilisons des Thymios composés de Raspberry PI pour nous permettre d'implémenter le robot.

## Rendus

Document  | Fichier correspondant
------------- | -------------
Cachier des charges  | [click here](https://gitlab.com/ProjetLachiheb/optimisation_deplacement_essaim_robots/blob/master/CahierDesCharges.pdf) 
Rapport  | [click here](https://gitlab.com/ProjetLachiheb/optimisation_deplacement_essaim_robots/blob/master/Rapport.pdf)
Recherche bibliographique | [click here](https://gitlab.com/ProjetLachiheb/optimisation_deplacement_essaim_robots/blob/master/Recherche_bibliographique.pdf)
Dossier test population 10 | [click here](https://gitlab.com/ProjetLachiheb/optimisation_deplacement_essaim_robots/blob/master/Dossier_test/Rapport_de_test_population_10.pdf)
Dossier test population 50 | [click here](https://gitlab.com/ProjetLachiheb/optimisation_deplacement_essaim_robots/blob/master/Dossier_test/Rapport_de_test_population_50.pdf)
Dossier de rapport de test | [click here](https://gitlab.com/ProjetLachiheb/optimisation_deplacement_essaim_robots/tree/master/Dossier_test)
Manuel utilisateur  | [click here](https://gitlab.com/ProjetLachiheb/optimisation_deplacement_essaim_robots/blob/master/Robocologie_manual.pdf)

## Code sources

Nom du dossier  | Dossier correspondant
------------- | -------------
Code sources centralisés | [click here](https://gitlab.com/ProjetLachiheb/optimisation_deplacement_essaim_robots/tree/master/Code%20sources%20centralis%C3%A9)
Code sources distribués  | [click here](https://gitlab.com/ProjetLachiheb/optimisation_deplacement_essaim_robots/tree/master/Code%20sources%20distribu%C3%A9s)
